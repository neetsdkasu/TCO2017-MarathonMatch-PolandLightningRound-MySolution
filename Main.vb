Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Try
            Dim S As Integer = CInt(Console.ReadLine())
            Dim board(S - 1) As String
            For i As Integer = 0 To UBound(board)
                board(i) = Console.ReadLine()
            Next i
            
            Dim ka As New KnightsAttacks()
            Dim ret() As String = ka.placeKnights(board)

            Console.WriteLine(ret.Length)
            For Each r As String In ret
                Console.WriteLine(r)
            Next r
            Console.Out.Flush()
            
        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module