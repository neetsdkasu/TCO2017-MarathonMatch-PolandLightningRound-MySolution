# アイデアのメモ

数が合うように貪欲にナイトを置いていく
(あるマスにナイトを置いたときに各攻撃マスへの数値がほどよくなるように配置？)

OR

いったん全部にナイトを埋めて
過剰なところから取り除いていく貪欲
(あるマスのナイトを取り除くことによって各攻撃マスの数値がほどよくなるように除去？)

の２つが大雑把に考えられる


焼きなまし法ってやつの典型問題っぽさがあるけど
焼きなまし法分からんので手はつけられん


あとは、boardの各位置からナイトが来うる位置全てに可能性を表す点数を足していって
各マスの点数が閾値以上ならナイトを置くという感じのもあるかな
これも貪欲法っぽいけど


------------------------------------------------------------

- - - - - - - - - - -
- - - A - B - - - - -
- - H - - - C - - - -
- - - - 8 - - - - - -
- - G - - - D - - - -
- - - F - E - - - - -
- - - - - - - - - - -

- - - - - - - - - - -
- - - A - B - - - - -
- - - - - - C - - - -
- - - - 7 - - - - - -
- - G - - - D - - - -
- - - F - E - - - - -
- - - - - - - - - - -

- - - - - - - - - - -
- - - A - B - - - - -
- - - - - - C - - - -
- - - - 6 - - - - - -
- - - - - - D - - - -
- - - F - E - - - - -
- - - - - - - - - - -

- - - - - - - - - - -
- - - A - B - - - - -
- - - - - - C - - - -
- - - - 5 - - - - - -
- - - - - - D - - - -
- - - - - E - - - - -
- - - - - - - - - - -


--------------------------------------------------------

ローカルとサーバーで約10倍弱のスペックの違いがあり
ローカルで収束する値とサーバーで収束する値がほとんど変わらないのなら
ローカルと同じ範囲の探索時間でサーバーで打ち止めて
残り時間はほかのアプローチができないかどうか

- たとえば1000msごとに探索範囲をせまくしていくとか(範囲外は切り捨てとか)











