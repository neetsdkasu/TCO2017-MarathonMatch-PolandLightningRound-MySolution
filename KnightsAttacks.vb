Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports XYD = System.Tuple(Of Integer, Integer, Integer)

Public Class KnightsAttacks
    Private Class Heap(Of V)
        Dim cmp As Comparison(Of V), values() As V, count As Integer
        Sub New(size As Integer, c As Comparison(Of V))
            ReDim values(size): cmp = c
        End Sub
        Sub Push(val As V)
            values(count) = val: count += 1: GoUp(count - 1)
        End SUb
        Function Pop() As V
            Dim t As V = values(0): count -= 1
            values(0) = values(count): GoDown(0): Return t
        End Function
        Sub Beam(size As Integer)
            If count > size Then count = size
        End Sub
        Function IsEmpty() As Boolean
            Return count = 0
        End Function
        Sub Swap(i As Integer, j As Integer)
            Dim t As V = values(i): values(i) = values(j): values(j) = t
        End Sub
        Sub GoUp(i As Integer)
            If i = 0 Then
                GoDown(i): Exit Sub
            End If
            Dim j As Integer = (i - 1) \ 2
            If cmp(values(i), values(j)) < 0 Then
                Swap(i, j): GoUp(j)
            Else
                GoDown(i)
            End If
        End Sub
        Sub GoDown(i As Integer)
            Dim j As Integer = i * 2 + 1
            Dim k As Integer = j + 1
            If j >= count Then Exit SUb
            If k < count Then
                If  cmp(values(j), values(k)) > 0 Then j = k
            End IF
            If cmp(values(i), values(j)) > 0 Then
                Swap(i, j): GoDown(j)
            End If
        End Sub
    End Class
    ' ----------------------------------------------------
    Dim rand As New Random(19831983)
    Dim H As Integer, W As Integer
    Dim field(,) As Integer
    Dim dx() As Integer = { 1,  2,  2,  1, -1, -2, -2, -1}
    Dim dy() As Integer = { 2,  1, -1, -2, -2, -1,  1,  2}
    Private Class Tp
        Public knights(,) As Boolean
        Public attacks(,) As Integer
        Public score As Integer
        Public cnt As Integer
        Sub New(k(,) As Boolean, a(,) As Integer, s As Integer)
            knights = k: attacks = a: score = s: cnt = 0
        End Sub
    End Class
    Public Function placeKnights(board() As String) As String()
        Dim time0 As Integer = Environment.TickCount + 9700
        Dim time1 As Integer = 0, time2 As Integer = time0 - 6000
        H = board.Length
        W = board(0).Length
        Dim DC As Integer = Math.Max(5, H \ 3)
        Dim DLM As Integer = Math.Min(H, 50)
        ReDim field(H - 1, W - 1)
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                field(i, j) = Asc(board(i)(j)) - Asc("0")
            Next j
        Next i
        Dim tpList As New List(Of Tp)()
        'tpList.Add(idea9_greedy())
        'tpList.Add(idea8_greedy())
        'tpList.Add(idea7_greedy())
        'tpList.Add(idea6_greedy())
        tpList.Add(idea5_greedy())
        tpList.Add(idea4_greedy())
        tpList.Add(idea3_greedy())
        tpList.Add(idea2_greedy())
        tpList.Add(idea1_greedy())
        tpList.Add(placeAtRandom())
        Dim ty As Integer = 0
        Dim knights(,) As Boolean = tpList(0).knights
        Dim score As Integer = tpList(0).score
        Do
            time1 = Environment.TickCount
            If time1 >= time0 Then Exit Do
            If time1 >= time2 Then
                W = Math.Max(DLM, W - DC)
                H = Math.Max(DLM, H - DC)
                time2 += 3000
            End If
            For tx As Integer = 0 To W - 1
                For Each t As Tp In tpList
                    climbingHill(t, tx, ty)
                Next t
            Next tx
            ty += 1
            If ty < H Then Continue Do
            ty = 0
            For i As Integer = 0 To tpList.Count - 1
                If tpList(i).cnt > 0 Then
                    tpList(i).cnt = 0
                    Continue For
                End If
                If tpList(i).score < score Then
                    knights = TryCast(tpList(i).knights.Clone(), Boolean(,))
                    score = tpList(i).score
                End If
                shuffle(tpList(i))
            Next i
        Loop
        For Each t As Tp In tpList
            If t.score < score Then
                knights = t.knights
                score = t.score
            End If
        Next t
        H = board.Length
        W = board(0).Length
        Dim ret(H - 1) As String
        For i As Integer = 0 To H - 1
            Dim s As String = ""
            For j As Integer = 0 To W - 1
                If knights(i, j) Then
                    s += "K"
                Else
                    s += "."
                End If
            Next j
            ret(i) = s
        Next i
        placeKnights = ret
    End Function
    Private Sub shuffle(data As Tp)
        Dim tmp(H, W) As Integer
        Dim tmpP(H, W) As Integer
        Dim tmpM(H, W) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim df As Integer = data.attacks(i, j) - field(i, j)
                tmp(i + 1, j + 1) = _
                    - tmp(i, j) + tmp(i + 1, j) + tmp(i, j + 1) _
                    + Math.Abs(df)
                tmpM(i + 1, j + 1) = _
                    - tmpM(i, j) + tmpM(i + 1, j) + tmpM(i, j + 1) _
                    + If(df < 0, Math.Abs(df),0)
                tmpP(i + 1, j + 1) = _
                    - tmpP(i, j) + tmpP(i + 1, j) + tmpP(i, j + 1) _
                    + If(df > 0, Math.Abs(df), 0)
            Next j
        Next i
        Dim L As Integer = Math.Max(5, Math.Min(H, W) \ 5)
        L = rand.Next(L - 1, L + 1)
        Dim pq As New Heap(Of XYD)(W + W + 10, Function(u As XYD, v As XYD) As Integer
            Return v.Item3.CompareTo(u.Item3)
        End Function)
        For i As Integer = 0 To H - L
            For j As Integer = 0 To W - L
                Dim td As Integer = tmp(i + L, j + L) _
                    + tmp(i, j) - tmp(i + L, j) - tmp(i, j + L)
                pq.Push(New XYD(j, i, td))
            Next j
            pq.Beam(W)
        Next i
        pq.Beam(rand.Next(8, 12))
        Do Until pq.IsEmpty()
            Dim v As XYD = pq.Pop()
            Dim x As Integer = v.Item1
            Dim y As Integer = v.Item2
            Dim tdM As Integer = tmpM(y + L, x + L) _
                + tmpM(y, x) - tmpM(y + L, x) - tmpM(y, x + L)
            Dim tdP As Integer = tmpP(y + L, x + L) _
                + tmpP(y, x) - tmpP(y + L, x) - tmpP(y, x + L)
            Dim flip As Boolean = tdM < tdP
            For i As Integer = y + 2 To y + L - 1 - 2
                For j As Integer = x + 2 To x + L - 1 - 2
                    If flip <> data.knights(i, j) Then Continue For
                    Dim chg As Integer = 1
                    If data.knights(i, j) Then chg = -1
                    data.knights(i, j) = Not data.knights(i, j)
                    For k As Integer = 0 To 7
                        Dim tx As Integer = j + dx(k)
                        If tx < 0 OrElse W <= tx Then Continue For
                        Dim ty As Integer = i + dy(k)
                        If ty < 0 OrElse H <= ty Then Continue For
                        Dim f As Integer = field(ty, tx)
                        Dim a As Integer = data.attacks(ty, tx)
                        data.score += Math.Abs(f - (a + chg)) - Math.Abs(f - a)
                        data.attacks(ty, tx) += chg
                    Next k
                Next j
            Next i
        Loop
    End Sub
    Private Sub climbingHill(data As Tp, tx As Integer, ty As Integer)
        Dim tmpScore As Integer = data.score
        Dim chg = 1
        If data.knights(ty, tx) Then chg = -1
        For k As Integer = 0 To 7
            Dim x As Integer = tx + dx(k)
            If x < 0 OrElse W <= x Then Continue For
            Dim y As Integer = ty + dy(k)
            If y < 0 OrElse H <= y Then Continue For
            Dim f As Integer = field(y, x)
            Dim a As Integer = data.attacks(y, x)
            tmpScore += Math.Abs(f - (a + chg)) - Math.Abs(f - a)
        Next k
        If tmpScore < data.score Then
            data.score = tmpScore
            data.knights(ty, tx) = Not data.knights(ty, tx)
            For k As Integer = 0 To 7
                Dim x As Integer = tx + dx(k)
                If x < 0 OrElse W <= x Then Continue For
                Dim y As Integer = ty + dy(k)
                If y < 0 OrElse H <= y Then Continue For
                data.attacks(y, x) += chg
            Next k
            data.cnt += 1
        End If
    End Sub
    Private Function calcScore(attacks(,) As Integer) As Integer
        Dim score As Integer = 0
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                score += Math.Abs(field(i, j) - attacks(i, j))
            Next j
        Next i
        calcScore = score
    End Function
    Private Function placeAtRandom() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If rand.Next(0, 100) < 20 Then Continue For
                ret(i, j) = True
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    tmp(y, x) += 1
                Next k
            Next j
        Next i
        placeAtRandom = New Tp(ret, tmp, calcScore(tmp))
    End Function
    Private Function idea1_greedy() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim c As Integer = 0
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    If tmp(y, x) + 1 <= field(y, x) Then 
                        c += 1
                    Else
                        c -= 1
                    End If
                Next k
                If c < 0 Then Continue For
                ret(i, j) = True
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    tmp(y, x) += 1
                Next k
            Next j
        Next i
        idea1_greedy = New Tp(ret, tmp, calcScore(tmp))
    End Function
    Private Function idea2_greedy() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            Dim ty As Integer = Math.Min(Math.Min(i, (H - 1) - i), 2)
            For j As Integer = 0 To W - 1
                ret(i, j) = True
                Dim tx As Integer = Math.Min(Math.Min(j, (W - 1) - j), 2)
                Select Case ty * 10 + tx
                Case 0
                    tmp(i, j) = 2
                Case 1, 10
                    tmp(i, j) = 3
                Case 11, 2, 20
                    tmp(i, j) = 4
                Case 12, 21
                    tmp(i, j) = 6
                Case 22
                    tmp(i, j) = 8
                End Select
            Next j
        Next i
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim c As Integer = 0
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    If tmp(y, x) - 1 >= field(y, x) Then 
                        c += 1
                    Else
                        c -= 1
                    End If
                Next k
                If c < 0 Then Continue For
                ret(i, j) = False
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    If tmp(y, x) > 0 Then tmp(y, x) -= 1
                Next k
            Next j
        Next i
        idea2_greedy = New Tp(ret, tmp, calcScore(tmp))
    End Function
    Private Function idea3_greedy() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        Dim worth(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim v As Integer = field(i, j) - 4
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    worth(y, x) += v
                Next k
            Next j
        Next i
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If worth(i, j) < 0 Then Continue For
                ret(i, j) = True
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    tmp(y, x) += 1
                Next k
            Next j
        Next i
        idea3_greedy = New Tp(ret, tmp, calcScore(tmp))
    End Function
    Private Function idea4_greedy() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If field(i, j) = 0 Then Continue For
                Dim c As Integer = 0
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    If ret(y, x) Then c += 1
                Next k
                If c >= field(i, j) Then Continue For
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    If ret(y, x) Then Continue For
                    ret(y, x) = True
                    c += 1
                    If c >= field(i, j) Then Exit For
                Next k
            Next j
        Next i
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If Not ret(i, j) Then Continue For
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    tmp(y, x) += 1
                Next k
            Next j
        Next i
        idea4_greedy = New Tp(ret, tmp, calcScore(tmp))
    End Function
    Private Function idea5_greedy() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If field(i, j) = 8 Then Continue For
                Dim c As Integer = 0
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    If Not ret(y, x) Then c += 1
                Next k
                If c <= field(i, j) Then Continue For
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    If ret(y, x) Then Continue For
                    ret(y, x) = True
                    c -= 1
                    If c <= field(i, j) Then Exit For
                Next k
            Next j
        Next i
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                ret(i, j) = Not ret(i, j)
                If Not ret(i, j) Then Continue For
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    tmp(y, x) += 1
                Next k
            Next j
        Next i
        idea5_greedy = New Tp(ret, tmp, calcScore(tmp))
    End Function
    Private Function idea6_greedy() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        Dim worth(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim v As Integer = (field(i, j) - 4) * 2
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    worth(y, x) += v
                Next k
            Next j
        Next i
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If worth(i, j) < 0 Then Continue For
                ret(i, j) = True
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    tmp(y, x) += 1
                Next k
            Next j
        Next i
        idea6_greedy = New Tp(ret, tmp, calcScore(tmp))
    End Function
    Private Function idea7_greedy() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        Dim worth(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim v As Integer = (field(i, j) - 4) * 3 \ 2
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    worth(y, x) += v
                Next k
            Next j
        Next i
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If worth(i, j) < 0 Then Continue For
                ret(i, j) = True
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    tmp(y, x) += 1
                Next k
            Next j
        Next i
        idea7_greedy = New Tp(ret, tmp, calcScore(tmp))
    End Function
    Private Function idea8_greedy() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        Dim worth(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim v As Integer = field(i, j) - 4
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    worth(y, x) += v
                Next k
            Next j
        Next i
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If worth(i, j) <= 0 Then Continue For
                ret(i, j) = True
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    tmp(y, x) += 1
                Next k
            Next j
        Next i
        idea8_greedy = New Tp(ret, tmp, calcScore(tmp))
    End Function
    Private Function idea9_greedy() As Tp
        Dim ret(H - 1, W - 1) As Boolean
        Dim tmp(H - 1, W - 1) As Integer
        Dim worth(H - 1, W - 1) As Integer
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                Dim v As Integer = field(i, j) - 4
                v = v * v * Math.Sign(v)
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    worth(y, x) += v
                Next k
            Next j
        Next i
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If worth(i, j) <= 0 Then Continue For
                ret(i, j) = True
                For k As Integer = 0 To 7
                    Dim x As Integer = j + dx(k)
                    If x < 0 OrElse W <= x Then Continue For
                    Dim y As Integer = i + dy(k)
                    If y < 0 OrElse H <= y Then Continue For
                    tmp(y, x) += 1
                Next k
            Next j
        Next i
        idea9_greedy = New Tp(ret, tmp, calcScore(tmp))
    End Function
End Class